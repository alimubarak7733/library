module.exports =  {
	testEnvironment: './jest/CustomEnvironment.ts',
	preset: 'ts-jest',
	transformIgnorePatterns: [ '<rootDir>/node_modules/' ],
	reporters: [ 'default', 'jest-junit' ],
	testResultsProcessor: 'jest-junit',
	collectCoverageFrom: [
		'lib/**',
		'!lib/interfaces/**',
	],
	coverageReporters: [ 'text', 'lcov' ],
	projects: [
		{
			preset: 'ts-jest',
			displayName: 'unit',
			testMatch: [ '<rootDir>/test/**/*.(spec|test).ts' ],
			testPathIgnorePatterns: [ '/test/e2e/' ],
			testEnvironment: './jest/CustomEnvironment.ts',
		},
		{
			preset: 'ts-jest',
			displayName: 'e2e',
			testMatch: [ '<rootDir>/test/e2e/**/*.(spec|test).ts' ],
		},
	],
};
