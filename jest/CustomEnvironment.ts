import NodeEnvironment from 'jest-environment-node';
import type {  Circus } from '@jest/types';

export default class CircusEnvironment extends NodeEnvironment
{
	async setup(): Promise<void>
	{
		await super.setup();
	}

	async teardown(): Promise<void>
	{
		await super.teardown();

		// unset global variable and mark for garbage collection
		this.global.currentlyRunningTest = undefined;
	}

	handleTestEvent(event: Circus.Event):void
	{
		// listen for the test start events and store the current test object in the global jest scope
		switch(event.name)
		{
			case 'test_start':
			case 'test_fn_start':
				this.global.currentlyRunningTest = event.test as Circus.TestEntry;
				break;
			default:
				break;
		}
	}
}
