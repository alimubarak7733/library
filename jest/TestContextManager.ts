// Circus is the default test runner used by Jest. See https://jestjs.io/blog/2021/05/25/jest-27#flipping-defaults
import type { Circus } from '@jest/types';

/**
 * Test Context Manager class for Jest.
 */
export default class TestContextManager<TestContextType>
{
	// key value map of test context data
	private contextStore: Map<string, TestContextType>;

	constructor()
	{
		// initialize the context store
		this.contextStore = new Map();
	}

	/**
	 * Returns the currently running test entry
	 * @returns the Circus test entry object
	 */
	getCurrentRunningTestEntry(): Circus.TestEntry
	{
		// returns the current test entry.
		// See the definition of `currentlyRunningTest` on the jest global object in CustomEnvironment.ts
		return (global as any).currentlyRunningTest as Circus.TestEntry;
	}

	/**
	 * @param  {string} key The key of the test context
	 * @returns The context object stored under the key.
	 * If no key is provided, the test name is used as the key.
	 */
	get(key: string):TestContextType
	{
		// fetch the context from the map
		const context = this.contextStore.get(key) || ({} as TestContextType);

		return context;
	}

	/**
	 * Returns the context object for the currently running test.
	 * @returns T
	 */
	getCurrentTestContext():TestContextType
	{
		// fetch the context from the store
		return this.get(this.getCurrentRunningTestEntry().name);
	}

	/**
	 * @param context The context object to add to the store
	 * @param key The key to store the context under.
	 * If not provided, the test name will be used
	 * @returns void
	 */
	set(key: string, context: TestContextType): void
	{
		// store the context in the store
		this.contextStore.set(key, context);
	}

	/**
	 * Stores the context object for the currently running test.
	 * @param  context The context object to add to the store
	 * @returns void
	 */
	setCurrentTestContext(context: TestContextType): void
	{
		// store the context in the store
		this.set(this.getCurrentRunningTestEntry().name, context);
	}

	/**
	 * To be called in the `afterEach` hook of a test.
	 * Returns true if the last test passed
	 * @returns boolean
	 */
	currentTestPassed(): boolean
	{
		// get the current test entry
		const currentlyRunningTest = this.getCurrentRunningTestEntry();

		// get the errors from the test entry
		const { errors } = currentlyRunningTest;

		// return true if there are no errors
		return !errors || errors.length === 0;
	}

	/**
	 * Log abstraction for test debug purposes
	 * @param  {any[]} ...args
	 * @returns void
	 */
	static log(...args: any[]): void
	{
		// eslint-disable-next-line no-console
		console.log(...args);
	}
}
