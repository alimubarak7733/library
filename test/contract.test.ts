/* eslint-disable no-console */
/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */

// Import testing libraries and utilities
import { createFakeBroadcastWithFakeLocktime, evaluateProgram, createProgram, meepProgram } from './test-util';

// Import Bitcoin Cash related interfaces
import { AuthenticationProgramBCH, AuthenticationErrorCommon, binToHex, hexToBin, flattenBinArray } from '@bitauth/libauth';

// Import AnyHedge library
import { AnyHedgeManager } from '../lib/anyhedge';
import { ContractData, ContractSettlement } from '../lib/interfaces';

// Import fixture data
import { ORACLE_PUBKEY, HEDGE_PUBKEY, LONG_PUBKEY, START_TIMESTAMP, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, ORACLE_WIF, CONTRACT_FUNDING_10M_BCH, MATURITY_TIMESTAMP, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_LIQUIDATION_TIMESTAMP, DEFAULT_LIQUIDATION_PRICE, DEFAULT_HEDGE_PAYOUT, DEFAULT_LONG_PAYOUT, DEFAULT_DURATION } from './fixture/constants';
import { liquidateFixtures, matureFixtures, truncLevelMaturationFixtures, modExtensionMaturationFixtures } from './fixture/contract.fixture';

// Import price oracle library.
import { OracleData } from '@generalprotocols/price-oracle';
import TestContextManager  from '../jest/TestContextManager';

// Specify the interface of our test context and define this in the type of `test()`
interface TestContext
{
	payoutProgram?: AuthenticationProgramBCH;
	matureOrLiquidateProgram?: AuthenticationProgramBCH;
}
const moduleTestContext = new TestContextManager<TestContext>();

// Load contract manager and swaps out the broadcast function

const loadContractManager = function(fakeLocktime?: number): AnyHedgeManager
{
	// Set up instance of AnyHedgeManager
	const manager = new AnyHedgeManager();

	// Stub the broadcast function to return the built transaction rather than broadcasting it
	// Also sets a provided fake locktime
	const fakeBroadcast = createFakeBroadcastWithFakeLocktime(fakeLocktime);
	jest.spyOn(manager, 'broadcastTransaction').mockImplementation(fakeBroadcast);

	return manager;
};

// Create contract data for specified hedge units, start price and protection
// Uses constant fixture values for all other parameters of manager.create()
const loadContractData = async function(
	manager: AnyHedgeManager,
	nominalUnits: number,
	startPrice: number,
	volatilityProtection: number,
	maxPriceIncrease: number,
): Promise<ContractData>
{
	const creationParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		hedgePublicKey: HEDGE_PUBKEY,
		longPublicKey: LONG_PUBKEY,
		nominalUnits: nominalUnits,
		startPrice: startPrice,
		startTimestamp: START_TIMESTAMP,
		duration: DEFAULT_DURATION,
		highLiquidationPriceMultiplier: 1 + maxPriceIncrease,
		lowLiquidationPriceMultiplier: 1 - volatilityProtection,
	};

	// During testing we use the createUnsafe() method to bypass safety checks
	const contractData = await manager.createContractUnsafe(creationParameters);

	return contractData;
};

// Define an interface for the return type of runLiquidate()/runMature()
interface ProgramAndSettlement
{
	program: AuthenticationProgramBCH;
	settlement: ContractSettlement;
}

// Run the liquidate() function for a specified contract with specified settlement message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runLiquidate = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
): Promise<ProgramAndSettlement>
{
	const liquidationParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractMetadata: contractData.metadata,
		contractParameters: contractData.parameters,
	};

	const settlement = await manager.liquidateContractFunding(liquidationParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.spendingTransaction, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the mature() function for a specified contract with specified settlement message
// and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runMature = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
): Promise<ProgramAndSettlement>
{
	const maturationParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractMetadata: contractData.metadata,
		contractParameters: contractData.parameters,
	};

	const settlement = await manager.matureContractFunding(maturationParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = await createProgram(manager, contractData, settlement.spendingTransaction, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return { program, settlement };
};

// Run the payout() function for a specified contract with specified settlement message and
// payout amounts, and returns a libauth Authentication Program.
// Uses constant fixture values for all other parameters
const runPayout = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
	hedgePayout: number,
	longPayout: number,
): Promise<AuthenticationProgramBCH>
{
	const payoutParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		settlementMessage: binToHex(settlementMessage),
		settlementSignature: binToHex(settlementSignature),
		previousMessage: binToHex(previousMessage),
		previousSignature: binToHex(previousSignature),
		hedgePayoutSats: hedgePayout,
		longPayoutSats: longPayout,
		contractFunding: CONTRACT_FUNDING_10M_BCH,
		contractParameters: contractData.parameters,
	};

	const payoutTxHex = await manager.automatedPayout(payoutParameters);

	// Create Authentication Program that can be evaluated with libauth
	const program = createProgram(manager, contractData, payoutTxHex, CONTRACT_FUNDING_10M_BCH.fundingSatoshis);

	return program;
};

// Specify the type of the runLiquidate() or runMature() functions above, so
// it can be passed into the runFixtureTest() function
type RunMatureOrLiquidateFunction = (
	manager: AnyHedgeManager,
	contractData: ContractData,
	settlementMessage: Uint8Array,
	settlementSignature: Uint8Array,
	previousMessage: Uint8Array,
	previousSignature: Uint8Array,
) => Promise<ProgramAndSettlement>;

// Runs liquidation/maturation test for a specific fixture
const runFixtureTest = async function(
	runMatureOrLiquidate: RunMatureOrLiquidateFunction,
	description: string,
	nominalUnits: number,
	startPrice: number,
	volatilityProtection: number,
	maxPriceIncrease: number,
	messageTimestamp: number,
	settlementPrice: number,
	hedgePayout: number,
	longPayout: number,
	expectedResult: (string | true),
): Promise<void>
{
	test(description, async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, nominalUnits, startPrice, volatilityProtection, maxPriceIncrease);

		// Set up test settlement message + signature
		const settlementMessage = await OracleData.createPriceMessage(messageTimestamp, 1, 2, settlementPrice);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);

		// Set up test previous message + signature
		const previousMessage = await OracleData.createPriceMessage(messageTimestamp - 10, 1, 1, settlementPrice);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// Execute the test using the payout function (does not perform checks so will not throw preemptively)
		const payoutProgram = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			hedgePayout,
			longPayout,
		);

		moduleTestContext.setCurrentTestContext({ payoutProgram });
		// Check that the script execution yields the correct result
		await expect(evaluateProgram(payoutProgram)).resolves.toEqual(expectedResult);

		if(expectedResult === true)
		{
			// Execute the test using the liquidate or mature function
			const { program: matureOrLiquidateProgram, settlement } = await runMatureOrLiquidate(
				manager,
				contractData,
				settlementMessage,
				settlementSignature,
				previousMessage,
				previousSignature,
			);

			// Check that the script execution yields the correct result
			moduleTestContext.setCurrentTestContext({ matureOrLiquidateProgram });

			await expect(evaluateProgram(matureOrLiquidateProgram)).resolves.toEqual(expectedResult);

			// Check that the returned settlement contains the expected data
			expect(settlement.hedgeSatoshis).toEqual(hedgePayout);
			expect(settlement.longSatoshis).toEqual(longPayout);
			expect(settlement.settlementMessage).toEqual(binToHex(settlementMessage));
			expect(settlement.settlementSignature).toEqual(binToHex(settlementSignature));
		}
		else
		{
			// Check that liquidate/mature function call fails since it performs checks before sending
			// so it should throw preemptively
			await expect(() => runMatureOrLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature))
				.rejects.toThrow();
		}
	});
};

// Test all fixture-based liquidation and maturation tests
liquidateFixtures.forEach((fixture) => runFixtureTest(runLiquidate, ...fixture));
matureFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));
truncLevelMaturationFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));
modExtensionMaturationFixtures.forEach((fixture) => runFixtureTest(runMature, ...fixture));

// Test additional cases that could not be included in the standardized fixtures
describe('contract tests', () =>
{
	// build the test context before each test
	beforeEach(() =>
	{
		// initialize the testContext entry for this test
		moduleTestContext.setCurrentTestContext({} as any);
	});

	afterEach(() =>
	{
		const testContext = moduleTestContext.getCurrentTestContext();

		// check test status. If the test has failed, print out the MEEP program of the failing test
		// we're only concerned with failing tests
		if(moduleTestContext.currentTestPassed()) return;

		if(testContext.matureOrLiquidateProgram)
		{
			TestContextManager.log('Meep the mature()/liquidate() call:');
			TestContextManager.log(meepProgram(testContext.matureOrLiquidateProgram));
		}
		else if(testContext.payoutProgram)
		{
			TestContextManager.log('Meep the payout() call:');
			TestContextManager.log(meepProgram(testContext.payoutProgram));
		}
	});

	test('should liquidate with default parameters (sanity check)', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function does not throw and the program evaluates successfully
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ matureOrLiquidateProgram: program });

		await expect(evaluateProgram(program)).resolves.toBeTruthy();
	});

	test('should mature with default parameters (sanity check)', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(MATURITY_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(MATURITY_TIMESTAMP - 10, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that mature() function does not throw and the program evaluates successfully
		const { program } = await runMature(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ matureOrLiquidateProgram: program });

		await expect(evaluateProgram(program)).resolves.toBeTruthy();
	});

	test('should fail when previous message is at or after maturity', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(MATURITY_TIMESTAMP + 10, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(MATURITY_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that mature() function throws as it makes this check preemptively
		await expect(() => runMature(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature)).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_HEDGE_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the contract's script fails
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.failedVerify);
	});

	test('should fail when settlement signature does not match settlement message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Intentionally create a signature over a different message than what is used
		// in the transaction, so the datasig check in the contract should fail
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const differentMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 3, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(differentMessage, ORACLE_WIF);

		// Use the correct signature for the previous message.
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function does not throw (because it does not check for signatures),
		// but the transaction still fails when sending it directly
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ matureOrLiquidateProgram: program });

		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
	});

	test('should fail when previous signature does not match previous message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Intentionally create a signature over a different message than what is used
		// in the transaction, so the datasig check in the contract should fail
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const differentMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 3, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(differentMessage, ORACLE_WIF);

		// Use the correct signature for the settlement message.
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);

		// check that liquidate() function does not throw (because it does not check for signatures),
		// but the transaction still fails when sending it directly
		const { program } = await runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ matureOrLiquidateProgram: program });

		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.nonNullSignatureFailure);
	});

	test('should fail when previous message is not immediately before the message used for settlement', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data where there is an intentional gap between the two messages
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 3, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature)).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_HEDGE_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the contract's script fails
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.failedVerify);
	});

	test('should fail when message is not a price message', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Create a test oracle message that specifies a negative price sequence, indicating it is a metadata message
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, -1, DEFAULT_LIQUIDATION_PRICE);

		// Create the rest of the test oracle data as usual
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, -2, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature)).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_HEDGE_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the contract's script fails
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.failedVerify);
	});

	test('should fail when sending incorrect amounts', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle data
		const settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// Intentionally set the hedge payout to an incorrect value
		const incorrectHedgePayout = DEFAULT_HEDGE_PAYOUT - 10;

		// Create Authentication Program that can be evaluated with libauth
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			incorrectHedgePayout,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the final statement of the contract is incorrect (which checks for outputs)
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.unsuccessfulEvaluation);
	});

	test('should fail when oracle price is zero', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle message
		let settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);

		// Manually replace the price part with 0
		const ZERO_PRICE = hexToBin('00000000');
		settlementMessage = flattenBinArray([ settlementMessage.slice(0, 12), ZERO_PRICE ]);

		// Set up rest of the test oracle data
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature)).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_HEDGE_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the contract's script fails
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.failedVerify);
	});

	test('should fail when oracle price is negative', async () =>
	{
		// Set up test contract
		const manager = loadContractManager();
		const contractData = await loadContractData(manager, DEFAULT_NOMINAL_UNITS, DEFAULT_START_PRICE, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER);

		// Set up test oracle message
		let settlementMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 2, DEFAULT_LIQUIDATION_PRICE);

		// Manually replace the price part with a negative value (0xffffffff is negative in Script's number format)
		const NEGATIVE_PRICE = hexToBin('ffffffff');
		settlementMessage = flattenBinArray([ settlementMessage.slice(0, 12), NEGATIVE_PRICE ]);

		// Set up rest of the test oracle data
		const settlementSignature = await OracleData.signMessage(settlementMessage, ORACLE_WIF);
		const previousMessage = await OracleData.createPriceMessage(DEFAULT_LIQUIDATION_TIMESTAMP, 1, 1, DEFAULT_LIQUIDATION_PRICE);
		const previousSignature = await OracleData.signMessage(previousMessage, ORACLE_WIF);

		// check that liquidate() function throws as it makes this check preemptively
		await expect(() => runLiquidate(manager, contractData, settlementMessage, settlementSignature, previousMessage, previousSignature)).rejects.toThrow();

		// Execute the transaction through the payout() function which does not do any checks
		const program = await runPayout(
			manager,
			contractData,
			settlementMessage,
			settlementSignature,
			previousMessage,
			previousSignature,
			DEFAULT_HEDGE_PAYOUT,
			DEFAULT_LONG_PAYOUT,
		);

		// store test context for later use
		moduleTestContext.setCurrentTestContext({ payoutProgram: program });

		// Check that the contract's script fails
		await expect(evaluateProgram(program)).resolves.toEqual(AuthenticationErrorCommon.failedVerify);
	});

	// This cannot be tested using the library (because it parses the oracle message), but we should test it in the contracts repo.
	test.todo('should fail when oracle timestamp is negative');

	// This cannot be tested using the library (because it enforces single inputs), but we should test it in the contracts repo.
	test.todo('should fail when multiple inputs are used in one transaction');

	// This cannot be tested using the library (because it automatically sends to the correct addresses), but we should test it in the contracts repo.
	test.todo('should fail when sending to incorrect addresses');

	// This cannot be tested using the library (because it automatically uses the correct data), but we should test it in the contracts repo.
	test.todo('should fail when providing incorrect payoutDataHash data');
});
