/* eslint-disable max-nested-callbacks */
import { AnyHedgeManager } from '../../lib';
import { HEDGE_WIF, ORACLE_WIF } from '../fixture/constants';
import { registerDefaultContractData } from '../test-util';

// This test file requires a settlement-services server running at localhost:6572 with token rate limiting turned off

// Define service domain, scheme and port to use for a localhost server.
const serviceScheme = 'http';
const serviceDomain = 'localhost';
const servicePort = 31157;

// Set up a manager without a token.
const managerWithoutToken = new AnyHedgeManager({ serviceScheme, serviceDomain, servicePort });

test('cannot request contract data for non-registered address', async () =>
{
	// Request a valid authentication token.
	const authenticationToken = await managerWithoutToken.requestAuthenticationToken('non-registered address');

	// Create a new manager using the requested token.
	const managerWithToken = new AnyHedgeManager({ authenticationToken, serviceScheme, serviceDomain, servicePort });

	// Use an address that has not been registered with the settlement service yet.
	const unregisteredAddress = 'bitcoincash:pp5s4jpe5pcjnl0ah2qxe7u5mwy63lz8r5ggm8tstg';

	// Try to retrieve contract details for that address and Check that it cannot find the contract.
	await expect(() => managerWithToken.getContractStatus(unregisteredAddress))
		.rejects.toThrowError('Failed to find contract');
});

test('cannot request contract data when different token than in registration', async () =>
{
	// Request two separate valid tokens.
	const registerToken = await managerWithoutToken.requestAuthenticationToken('register');
	const getStatusToken = await managerWithoutToken.requestAuthenticationToken('getStatus');

	// Create two separate managers with these tokens.
	const registerManager = new AnyHedgeManager({ authenticationToken: registerToken, serviceScheme, serviceDomain, servicePort });
	const getStatusManager = new AnyHedgeManager({ authenticationToken: getStatusToken, serviceScheme, serviceDomain, servicePort });

	// Register a contract with the first token.
	const contractData = await registerDefaultContractData(registerManager);

	// Try to retrieve contract details with the second token Check that the contract details are not returned.
	await expect(() => getStatusManager.getContractStatus(contractData.address))
		.rejects.toThrowError('Failed to find contract');
});

test('cannot request contract data with WIF that is not party to the contract', async () =>
{
	// Request two separate valid tokens.
	const registerToken = await managerWithoutToken.requestAuthenticationToken('register');
	const getStatusToken = await managerWithoutToken.requestAuthenticationToken('getStatus');

	// Create two separate managers with these tokens.
	const registerManager = new AnyHedgeManager({ authenticationToken: registerToken, serviceScheme, serviceDomain, servicePort });
	const getStatusManager = new AnyHedgeManager({ authenticationToken: getStatusToken, serviceScheme, serviceDomain, servicePort });

	// Register a contract with the first token.
	const contractData = await registerDefaultContractData(registerManager);

	// Try to retrieve contract details with the second token and a WIF string that is not party to the contract.
	// Check that the contract details are not returned.
	await expect(() => getStatusManager.getContractStatus(contractData.address, ORACLE_WIF))
		.rejects.toThrowError('Failed to provide contract');
});

test('can request contract data with token that was used in registration', async () =>
{
	// Request a valid token.
	const authenticationToken = await managerWithoutToken.requestAuthenticationToken('register');

	// Create a new manager that uses this token.
	const managerWithToken = new AnyHedgeManager({ authenticationToken, serviceScheme, serviceDomain, servicePort });

	// Register a new contract using this token.
	const contractData = await registerDefaultContractData(managerWithToken);

	// Retrieve contract details using the same token.
	const retrievedData = await managerWithToken.getContractStatus(contractData.address);

	// Remove funding and settlement properties as these are not present in the registration data.
	delete retrievedData.funding;
	delete retrievedData.settlement;

	// Check that the retrieved data matches the stored data.
	expect(retrievedData).toEqual(contractData);
});

test('can request contract data with WIF that is party to the contract', async () =>
{
	// Request two separate valid tokens.
	const registerToken = await managerWithoutToken.requestAuthenticationToken('register');
	const getStatusToken = await managerWithoutToken.requestAuthenticationToken('getStatus');

	// Create two separate managers with these tokens.
	const registerManager = new AnyHedgeManager({ authenticationToken: registerToken, serviceScheme, serviceDomain, servicePort });
	const getStatusManager = new AnyHedgeManager({ authenticationToken: getStatusToken, serviceScheme, serviceDomain, servicePort });

	// Register a contract with the first token.
	const contractData = await registerDefaultContractData(registerManager);

	// Retrieve contract details using the second token, but providing a valid WIF of one of the contract's parties.
	const retrievedData = await getStatusManager.getContractStatus(contractData.address, HEDGE_WIF);

	// Remove funding and settlement properties as these are not present in the registration data.
	delete retrievedData.funding;
	delete retrievedData.settlement;

	// Check that the retrieved data matches the stored data.
	expect(retrievedData).toEqual(contractData);
});
