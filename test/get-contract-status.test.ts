/* eslint-disable max-nested-callbacks */
/* eslint-disable no-param-reassign */
/* eslint-disable import/first */

// Fetch mocks need to be enabled before all other imports
import { enableFetchMocks } from 'jest-fetch-mock';

enableFetchMocks();

// Import testing libraries and utilities
import { loadDefaultContractData } from './test-util';

// Import AnyHedge library
import { AnyHedgeManager } from '../lib/anyhedge';

describe('getContractStatus()', () =>
{
	let manager: AnyHedgeManager;

	beforeEach(async () =>
	{
		// Reset any 'fetch' mocks
		(fetch as any).resetMocks();

		// Load a contract manager
		manager = new AnyHedgeManager({ authenticationToken: 'DUMMY' });
	});

	test('should fail when the settlement service responds with data for a different address', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract data very slightly, resulting in a different address
		const contractCreationParameters = { ...contractData.metadata, duration: contractData.metadata.duration + 10 };
		const contractDataForDifferentAddress = await manager.createContract(contractCreationParameters);

		// Make the settlement service respond with contract data for that address
		(fetch as any).mockResponseOnce(JSON.stringify(contractDataForDifferentAddress));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is for a different contract
		await expect(getContractStatusCall).rejects.toThrow(/Received contract data for contract .+ while requesting contract data for contract/);
	});

	test('should fail when settlement service responds with invalid contract metadata', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract metadata very slightly, without changing the address, meaning that
		// the contract data is invalid for the linked address
		const differentContractData =
		{
			...contractData,
			metadata:
			{
				...contractData.metadata,
				duration: contractData.metadata.duration + 10,
			},
		};

		// Make the settlement service respond with that invalid contract data
		(fetch as any).mockResponseOnce(JSON.stringify(differentContractData));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is invalid
		await expect(getContractStatusCall).rejects.toThrow(/Invalid contract data received for contract/);
	});

	test('should fail when settlement service responds with invalid contract parameters', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Change the contract parameters very slightly, without changing the address, meaning that
		// the contract data is invalid for the linked address
		const differentContractData =
		{
			...contractData,
			parameters:
			{
				...contractData.parameters,
				maturityTimestamp: contractData.parameters.maturityTimestamp + 10,
			},
		};

		// Make the settlement service respond with that invalid contract data
		(fetch as any).mockResponseOnce(JSON.stringify(differentContractData));

		// Attempt to get contract data
		const getContractStatusCall = manager.getContractStatus(contractData.address);

		// Expect the function call to fail and report that the data is invalid
		await expect(getContractStatusCall).rejects.toThrow(/Invalid contract data received for contract/);
	});

	test('should succeed', async () =>
	{
		// Load default contract data
		const contractData = await loadDefaultContractData(manager);

		// Make the settlement service respond with that contract data
		(fetch as any).mockResponseOnce(JSON.stringify(contractData));

		// Get contract data
		const returnedData = await manager.getContractStatus(contractData.address);

		// Expect the returned contract data to match the stored contract data
		expect(returnedData).toEqual(contractData);
	});
});
