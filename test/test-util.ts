import { Transaction } from 'cashscript';
import { AuthenticationProgramBCH, instantiateVirtualMachineBCH, Output, hexToBin, binToHex, encodeTransaction, decodeTransactionUnsafe, bigIntToBinUint64LE, binToBigIntUint64LE, AddressType, addressContentsToLockingBytecode } from '@bitauth/libauth';
import { AnyHedgeManager } from '../lib/anyhedge';
import { ContractData, ContractCreationParameters } from '../lib/interfaces';
import { addressToLockScript, hash160 } from '../lib/util/bitcoincash-util';
import { PROD_ORACLE_PUBKEY, DEFAULT_NOMINAL_UNITS, DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER, DEFAULT_START_PRICE, HEDGE_PUBKEY, LONG_PUBKEY, ORACLE_PUBKEY, START_TIMESTAMP, DEFAULT_VOLATILITY_PROTECTION, DEFAULT_DURATION } from './fixture/constants';

// Create a fake version of the contract.broadcast() function that sets the transaction's
// locktime to a specific value and returns the built transaction
export type BroadcastFunction = (transactionBuilder: Transaction) => Promise<string>;
export const createFakeBroadcastWithFakeLocktime = function(locktime?: number): BroadcastFunction
{
	const fakeBroadcast = async (transactionBuilder: Transaction): Promise<string> =>
	{
		// If a custom locktime is provided we set it.
		if(locktime)
		{
			transactionBuilder.withTime(locktime);
		}

		// Build the transaction rather than broadcasting.
		return transactionBuilder.build();
	};

	return fakeBroadcast;
};

// Create a fake version of the bitcoincash-util::broadcastTransaction function that
// returns the transaction without broadcasting
export type BroadcastTransactionFunction = (transactionHex: string) => Promise<string>;
export const createFakeBroadcastTransaction = function(): BroadcastTransactionFunction
{
	const fakeBroadcast =
		async (transactionHex: string): Promise<string> => transactionHex;

	return fakeBroadcast;
};

// Create a libauth compatible source output to use in the Authentication Program for P2PKH
export const createSourceOutputP2PKH = async function(
	publicKeyHex: string,
	satoshis: number,
): Promise<Output>
{
	const publicKeyHashBin = await hash160(hexToBin(publicKeyHex));
	const addressContents = { payload: publicKeyHashBin, type: AddressType.p2pkh };
	const lockingBytecode = addressContentsToLockingBytecode(addressContents);
	const sourceOutput = { lockingBytecode, satoshis: bigIntToBinUint64LE(BigInt(satoshis)) };

	return sourceOutput;
};

// Create a libauth compatible Authentication Program for P2PKH (for a single input with provided index) to be evaluated
export const createProgramP2PKH = async function(
	transactionHex: string,
	publicKeyHex: string,
	satoshis: number,
	inputIndex: number = 0,
): Promise<AuthenticationProgramBCH>
{
	const sourceOutput = await createSourceOutputP2PKH(publicKeyHex, satoshis);
	const spendingTransaction = decodeTransactionUnsafe(hexToBin(transactionHex));
	const program = { inputIndex, sourceOutput, spendingTransaction };

	return program;
};

// Create a libauth compatible source output to use in the Authentication Program
export const createSourceOutput = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	satoshis: number,
): Promise<Output>
{
	const contractInstance = await manager.compileContract(contractData.parameters);
	const lockingBytecode = hexToBin(addressToLockScript(contractInstance.address));
	const sourceOutput = { lockingBytecode, satoshis: bigIntToBinUint64LE(BigInt(satoshis)) };

	return sourceOutput;
};

// Create a libauth compatible Authentication Program (for a single input with provided index) to be evaluated
export const createProgram = async function(
	manager: AnyHedgeManager,
	contractData: ContractData,
	transactionHex: string,
	satoshis: number,
	inputIndex: number = 0,
): Promise<AuthenticationProgramBCH>
{
	const sourceOutput = await createSourceOutput(manager, contractData, satoshis);
	// Note: we use decodeTransactionUnsafe() because we know the input transaction hex is valid
	const spendingTransaction = decodeTransactionUnsafe(hexToBin(transactionHex));
	const program = { inputIndex, sourceOutput, spendingTransaction };

	return program;
};

// Evaluate a libauth Authentication Program on a libauth VM
export const evaluateProgram = async function(program: AuthenticationProgramBCH): Promise<string | true>
{
	const vm = await instantiateVirtualMachineBCH();
	const finalState = vm.evaluate(program);
	const result = vm.verify(finalState);

	return result;
};

// Generates the meep command to debug a libauth Authentication Program
export const meepProgram = function(program: AuthenticationProgramBCH): string
{
	// Serialize the spending transaction from a Transaction object to a hex string
	const transactionHex = binToHex(encodeTransaction(program.spendingTransaction));

	// Extract the input index from the program
	const { inputIndex } = program;

	// Serialize the input's amount from a 64-bit little endian Uint8Array to a number
	const inputAmount = binToBigIntUint64LE(program.sourceOutput.satoshis);

	// Serialize the locking script from a Uint8Array to a hex string
	const inputLockingScript = binToHex(program.sourceOutput.lockingBytecode);

	return `meep debug --tx=${transactionHex} --idx=${inputIndex} --amt=${inputAmount} --pkscript=${inputLockingScript}`;
};

// Create contract data filled with default fixture data
export const loadDefaultContractData = async function(
	manager: AnyHedgeManager,
	overrides: Partial<ContractCreationParameters> = {},
): Promise<ContractData>
{
	const defaultCreationParameters =
	{
		oraclePublicKey: ORACLE_PUBKEY,
		hedgePublicKey: HEDGE_PUBKEY,
		longPublicKey: LONG_PUBKEY,
		nominalUnits: DEFAULT_NOMINAL_UNITS,
		startPrice: DEFAULT_START_PRICE,
		startTimestamp: START_TIMESTAMP,
		duration: DEFAULT_DURATION,
		highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
	};

	const creationParameters = { ...defaultCreationParameters, ...overrides };

	// Create a contract using the default parameters.
	const contractData = await manager.createContract(creationParameters);

	return contractData;
};

// Register a contract with default fixture data (and a random start timestamp)
export const registerDefaultContractData = async function(
	manager: AnyHedgeManager,
): Promise<ContractData>
{
	// Generate a random start timestamp (so every new contract is unique)
	const startTimestamp = Math.round(Math.random() * 100000000);

	const registrationParameters =
	{
		oraclePublicKey: PROD_ORACLE_PUBKEY,
		hedgePublicKey: HEDGE_PUBKEY,
		longPublicKey: LONG_PUBKEY,
		nominalUnits: DEFAULT_NOMINAL_UNITS,
		startPrice: DEFAULT_START_PRICE,
		startTimestamp: startTimestamp,
		duration: DEFAULT_DURATION,
		highLiquidationPriceMultiplier: DEFAULT_HIGH_LIQUIDATION_PRICE_MULTIPLIER,
		lowLiquidationPriceMultiplier: 1 - DEFAULT_VOLATILITY_PROTECTION,
	};

	// Register a contract using the default parameters.
	const contractData = await manager.registerContractForSettlement(registrationParameters);

	return contractData;
};
