/* eslint-disable max-nested-callbacks */
// Allow line comments so that the ContractFixture definition is more readable
/* eslint-disable line-comment-position, no-inline-comments, prefer-regex-literals, @typescript-eslint/comma-dangle */

import { AnyHedgeManager } from '../../lib';
import {
	DUST_LIMIT,
	MAX_HIGH_LIQUIDATION_PRICE,
	MAX_START_PRICE, MIN_NOMINAL_UNITS,
	MIN_LOW_LIQUIDATION_PRICE,
	MIN_START_PRICE, SATS_PER_BCH,
} from '../../lib/constants';

// Create an instance of the contract manager.
const contractManager = new AnyHedgeManager();

// Sane default parameters regarding price for creating a contract
const SOME_TIMESTAMP = 1620000000;
const SOME_DURATION = 0;
const SOME_PUBKEY_HEX = '000000000000000000000000000000000000000000000000000000000000000000';
const SOME_NOMINAL_UNITS = 10000;
const SOME_START_PRICE = 1000;
const SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER = 1.1;
const SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER = 0.9;

// Derived boundary parameters
const highLiquidationMultiplierForMaxPrice = MAX_HIGH_LIQUIDATION_PRICE / SOME_START_PRICE;
const lowLiquidationMultiplierForMinPrice = MIN_LOW_LIQUIDATION_PRICE / SOME_START_PRICE;
const highLiquidationMultiplierForExtremeLong = (MIN_NOMINAL_UNITS * SATS_PER_BCH) / (DUST_LIMIT * SOME_START_PRICE);

type ContractFixture =
[
	string,               // description
	number,               // nominalUnits
	number,               // startPrice
	number,               // highLiquidationPriceMultiplier
	number,               // lowLiquidationPriceMultiplier
	('safe' | 'unsafe'),  // which creation function to use
	(RegExp | null),      // expectedErrorRegExp
];

// These cases test price constraints.
// safe creation tests all precision and redeemability conditions.
// unsafe creation tests only hard redeemability conditions.
const priceConstraintFixtures: ContractFixture[] = [
	// start price range
	[ 'should fail over max start price',  SOME_NOMINAL_UNITS,  0.1 + MAX_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_START_PRICE}`) ],
	[ 'should succeed at max start price', SOME_NOMINAL_UNITS,        MAX_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should succeed at min start price', SOME_NOMINAL_UNITS,        MIN_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should fail below min start price', SOME_NOMINAL_UNITS, -0.1 + MIN_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MIN_START_PRICE}`) ],

	// liquidation price range
	[ 'should fail over max high liquidation price',            SOME_NOMINAL_UNITS, SOME_START_PRICE, 0.1 + highLiquidationMultiplierForMaxPrice,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_HIGH_LIQUIDATION_PRICE}`) ],
	[ 'should succeed at max high liquidation price',           SOME_NOMINAL_UNITS, SOME_START_PRICE,       highLiquidationMultiplierForMaxPrice,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should succeed with high liquidation price over start',  SOME_NOMINAL_UNITS, SOME_START_PRICE,                                        1.1,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', null ],
	[ 'should fail with high liquidation price equal to start', SOME_NOMINAL_UNITS, SOME_START_PRICE,                                        1.0,              SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp('immediate liquidation') ],
	[ 'should fail with low liquidation price equal to start',  SOME_NOMINAL_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                                                1.0, 'safe', new RegExp('immediate liquidation') ],
	[ 'should succeed with low liquidation price under start',  SOME_NOMINAL_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                                                0.9, 'safe', null ],
	[ 'should succeed at min low liquidation price',            SOME_NOMINAL_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,                lowLiquidationMultiplierForMinPrice, 'safe', null ],
	[ 'should fail under min low liquidation price',            SOME_NOMINAL_UNITS, SOME_START_PRICE,     SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,         -0.1 + lowLiquidationMultiplierForMinPrice, 'safe', new RegExp(`${MIN_LOW_LIQUIDATION_PRICE}`) ],

	// liquidation price limit due to coupling with minimum payout sats in extreme long case
	[ 'should fail at extreme long high liquidation price limit '
		+ 'but fail because the lowest extreme liquidation price limit '
		+ 'is higher than the absolute max liquidation price limit', MIN_NOMINAL_UNITS, SOME_START_PRICE, -0.0001 + highLiquidationMultiplierForExtremeLong, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp(`${MAX_HIGH_LIQUIDATION_PRICE}`) ],
	[ 'should fail over extreme long high liquidation price limit',  MIN_NOMINAL_UNITS, SOME_START_PRICE,     0.1 + highLiquidationMultiplierForExtremeLong, SOME_LOW_LIQUIDATION_PRICE_MULTIPLIER, 'safe', new RegExp('and high liquidation price multiplier') ],

	// liquidation min max
	[ 'should succeed with low liquidation price over zero', SOME_NOMINAL_UNITS,  SOME_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER, 0.001, 'unsafe', null ],
	[ 'should fail with low liquidation price at zero',      SOME_NOMINAL_UNITS,  SOME_START_PRICE, SOME_HIGH_LIQUIDATION_PRICE_MULTIPLIER,   0.0, 'unsafe', new RegExp('cannot be <= 0') ],
];

// Runs test on price constraints for a given fixture
const runPriceConstraintTest = async function(
	_description: string,
	nominalUnits: number,
	startPrice: number,
	highLiquidationPriceMultiplier: number,
	lowLiquidationPriceMultiplier: number,
	whichCreationFunction: ('safe' | 'unsafe'),
	expectedErrorRegExp: (RegExp | null),
): Promise<void>
{
	const creationParameters =
	{
		oraclePublicKey: SOME_PUBKEY_HEX,
		hedgePublicKey: SOME_PUBKEY_HEX,
		longPublicKey: SOME_PUBKEY_HEX,
		nominalUnits: nominalUnits,
		startPrice: startPrice,
		startTimestamp: SOME_TIMESTAMP,
		duration: SOME_DURATION,
		highLiquidationPriceMultiplier: highLiquidationPriceMultiplier,
		lowLiquidationPriceMultiplier: lowLiquidationPriceMultiplier,
	};

	// Choose to use create() or unsafeCreate()
	const creationFunction = (whichCreationFunction === 'safe') ? contractManager.createContract : contractManager.createContractUnsafe;

	// Confirm that the test succeeds or fails as expected
	if(expectedErrorRegExp === null)
	{
		// Confirm that the creation works fine when no error is expected
		await expect(creationFunction.call(contractManager, creationParameters))
			.resolves.not.toThrow();
	}
	else
	{
		// Confirm that the error is as expected when creation should fail
		await expect(() => creationFunction.call(contractManager, creationParameters))
			.rejects.toThrowError(expectedErrorRegExp);
	}
};

// Run all test fixtures
test.each(priceConstraintFixtures)('%s', runPriceConstraintTest);
