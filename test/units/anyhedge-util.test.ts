/* eslint-disable max-nested-callbacks */
import { binToHex, encodeTransaction, hexToBin } from '@bitauth/libauth';
import { isParsedMutualRedemptionData, isParsedPayoutData, SettlementType, AnyHedgeManager, SettlementParseError } from '../../lib';
import { estimatePayoutTransactionFee, parseSettlementTransaction } from '../../lib/util/anyhedge-util';
import { loadDefaultContractData } from '../test-util';

describe('(estimatePayoutTransactionFee()', () =>
{
	test('should estimate payout transaction size', async () =>
	{
		// Set up default test data.
		const manager = new AnyHedgeManager();
		const contractData = await loadDefaultContractData(manager);

		// Compile the contract for the default contract data.
		const contract = await manager.compileContract(contractData.parameters);

		// Estimate the payout transaction size.
		// Check that the payout transaction size matches the expectations
		await expect(estimatePayoutTransactionFee(contract, contractData.parameters, 1.0)).resolves.toEqual(1347);
	});
});

describe('parseSettlementTransaction()', () =>
{
	// Retrieved from an actual payout transaction of a test contract.
	// https://blockchair.com/bitcoin-cash/transaction/ac0a3e8ca56c53c212b8fc2829b0716b74e5af89e3a3d4e074f9f8a6cd0f393b
	const payoutTransaction =
	{
		inputs: [
			{
				outpointIndex: 0,
				outpointTransactionHash: hexToBin('a87ec10588d8d2a31cfc8be76b71bbcc6abf06645667305a1939ad82a69323f0'),
				sequenceNumber: 4294967294,
				unlockingBytecode: hexToBin('40bb8e023d53ec4ed9011fb2b21da9f2363880292fd73e65895f8d1d42d34f9c999fd28f6d7e18b803c75c1a3508431ff15882ed6af94262b36cb9cde717ddeaf5101c6ef160010000000100000050c30000402f9dc049b87e4f695b4ba19f57df42d44ecc27cff38a52f25d4cc537043278d805a582e0a147d580d8afa2b4cc73d8caa774d8829b3d29bced5027cb0cc2578b10306ef160020000000200000050c3000021029034aa3a3bfb07a47bd3acb160d39f61c97c0be087c8864c96fdf88fafa3330141ca3eb7641d61001bbf90ff1a437d6bdd121acbaf15142659d86a51be11c3d5cf409b38f0f6f953c7c4c3694913b146dfe42d6d5af7c11ab3b21a155df4c66070412103e4a5b4a1d9365492dfcad9a05adcdea259acb51a20eabb84584f7aa3c7009ebe1a1976a91469981c844c2055c94700568f2377515f48503ba288ac1a1976a914e4ebd82d22fc616a9f6b17d24e53dd22fe3de69a88ac4d000202000000b190124c6154bbb65811e64175abc943ee30632df8d97dacc1ef49f06b6a50f318606b350cd8bf565266bc352f0caddcf01e8fa789dd8a15386327cf8cabe198f02393a682ad39195a3067566406bf6accbb716be78bfc1ca3d2d88805c17ea800000000fd610104306ef16004b86df1600320a107037c920000010003d6080204f2052a015114ab5b587c9f9c80ae59bd6bd27fcb9f161a12380b1418e9093fd24ba361d2d8b1b7c44d13245a963a055b79009c637b695b795d797ea9885c7a5b7aad5b7a5b7aad6d6d6d6d6d51675b7a519d5b79547f7701207f01207f7701247f820128947f7701207f757b7baa887b5d795f797e60797ea9885f7a607a6e7c828c7f755f7aa87bbbad01117a0111795f79bb5f7a5f795f7abb5e79587f77547f75817600a0695e79587f77547f75818c9d5e7a547f75815b799f695d795c7f77817600a0695979a35879a45e7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b695479789657797c54807e81557a527997577a827754527982779478a3007880537a54807e817b7b94537a7c7f778196935479a3547a789402220258805857798277945779547a5279807e527985587a7e577a547a537a807e7b85557a7e7eaa877777685b12020000000000fefffffffe3a0756a0aca6ff1374e868d240e130f3bb2f2bcf11b8f078c55f958a5a29e50000000041000000514d610104306ef16004b86df1600320a107037c920000010003d6080204f2052a015114ab5b587c9f9c80ae59bd6bd27fcb9f161a12380b1418e9093fd24ba361d2d8b1b7c44d13245a963a055b79009c637b695b795d797ea9885c7a5b7aad5b7a5b7aad6d6d6d6d6d51675b7a519d5b79547f7701207f01207f7701247f820128947f7701207f757b7baa887b5d795f797e60797ea9885f7a607a6e7c828c7f755f7aa87bbbad01117a0111795f79bb5f7a5f795f7abb5e79587f77547f75817600a0695e79587f77547f75818c9d5e7a547f75815b799f695d795c7f77817600a0695979a35879a45e7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b695479789657797c54807e81557a527997577a827754527982779478a3007880537a54807e817b7b94537a7c7f778196935479a3547a789402220258805857798277945779547a5279807e527985587a7e577a547a537a807e7b85557a7e7eaa87777768'),
			},
		],
		locktime: 0,
		outputs: [
			{
				lockingBytecode: hexToBin('76a914e4ebd82d22fc616a9f6b17d24e53dd22fe3de69a88ac'),
				satoshis: hexToBin('a286010000000000'),
			},
			{
				lockingBytecode: hexToBin('76a91469981c844c2055c94700568f2377515f48503ba288ac'),
				satoshis: hexToBin('3682000000000000'),
			},
		],
		version: 2,
	};
	// Retrieved from an actual mutual redemption transaction of a test contract.
	// https://blockchair.com/bitcoin-cash/transaction/35a53df7dc5e4156d676f781f829eb26f7e759301da4285844c164a9be12e841
	const mutualRedemptionTransaction =
	{
		inputs: [
			{
				outpointIndex: 0,
				outpointTransactionHash: hexToBin('f4e070549dd9470e730e4200ab16e65a0bee7709ccd6d8401bb2fa29821b1ac1'),
				sequenceNumber: 0,
				unlockingBytecode: hexToBin('4186cf9d2520a651a70375d16480af59124f8a70f4c792e5b9693bf6e60483caa12543e860eb011d3e9844e603402631320ee45385a565efd931a76f4fe608dbb24141e460e7de1f62ce135b1effae38f82785801675d10b17cdb8935b41585141f3c1a5ce6dee64386ba925d41d592054f67c47870dba53f1eb4f8ff1d83ee5015d814121032b7d6df675bcd482d1a19b99ea39cda3c89484a6e85278ac6a2f69029db714902102934d87d10701d700c06ab721c98b2a700b01db80bcd5c1ccf8cbd955c6002ef6004d610104306ef16004b86df1600320a107037c920000010003d6080204f2052a015114ab5b587c9f9c80ae59bd6bd27fcb9f161a12380b1418e9093fd24ba361d2d8b1b7c44d13245a963a055b79009c637b695b795d797ea9885c7a5b7aad5b7a5b7aad6d6d6d6d6d51675b7a519d5b79547f7701207f01207f7701247f820128947f7701207f757b7baa887b5d795f797e60797ea9885f7a607a6e7c828c7f755f7aa87bbbad01117a0111795f79bb5f7a5f795f7abb5e79587f77547f75817600a0695e79587f77547f75818c9d5e7a547f75815b799f695d795c7f77817600a0695979a35879a45e7a547f7581765c7aa2695b7aa2785a7a8b5b7aa5919b695479789657797c54807e81557a527997577a827754527982779478a3007880537a54807e817b7b94537a7c7f778196935479a3547a789402220258805857798277945779547a5279807e527985587a7e577a547a537a807e7b85557a7e7eaa87777768'),
			},
		],
		locktime: 0,
		outputs: [
			{
				lockingBytecode: hexToBin('76a914e4ebd82d22fc616a9f6b17d24e53dd22fe3de69a88ac'),
				satoshis: hexToBin('a086010000000000'),
			},
			{
				lockingBytecode: hexToBin('76a91469981c844c2055c94700568f2377515f48503ba288ac'),
				satoshis: hexToBin('3682000000000000'),
			},
		],
		version: 2,
	};

	test('should throw when transaction contains more than one input', async () =>
	{
		// Duplicate the inputs, so that there are 2 inputs rather than 1.
		const inputs = payoutTransaction.inputs.concat(payoutTransaction.inputs);
		const transaction = { ...payoutTransaction, inputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain two outputs', async () =>
	{
		// Only use the first output, so that there is 1 output rather than 2.
		const outputs = [ payoutTransaction.outputs[0] ];
		const transaction = { ...payoutTransaction, outputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should throw when transaction does not contain expected input parameters', async () =>
	{
		// Remove the unlocking bytecode from the transaction input so it does not contain any input parameters.
		const inputs = [{ ...payoutTransaction.inputs[0], unlockingBytecode: hexToBin('') }];
		const transaction = { ...payoutTransaction, inputs };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Attempt to parse the settlement transaction.
		// Check that the call throws a SettlementParseError.
		await expect(() => parseSettlementTransaction(encodedTransaction)).rejects.toThrowError(SettlementParseError);
	});

	test('should successfully parse a mutual redemption transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction = { ...mutualRedemptionTransaction };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Define the expected data.
		const expectedSettlementData =
		{
			address: 'bitcoincash:pzn6kmrshz9qhzv6wpamagwpxefpwg2hhu09slsn70',
			funding: {
				fundingTransaction: 'f4e070549dd9470e730e4200ab16e65a0bee7709ccd6d8401bb2fa29821b1ac1',
				fundingOutput: 0,
			},
			parameters: {
				lowLiquidationPrice: 37500,
				highLiquidationPrice: 500000,
				startTimestamp: 1626435000,
				maturityTimestamp: 1626435120,
				hedgeMutualRedeemPublicKey: '02934d87d10701d700c06ab721c98b2a700b01db80bcd5c1ccf8cbd955c6002ef6',
				longMutualRedeemPublicKey: '032b7d6df675bcd482d1a19b99ea39cda3c89484a6e85278ac6a2f69029db71490',
				lowTruncatedZeroes: '',
				highLowDeltaTruncatedZeroes: '00',
				nominalUnitsXSatsPerBchHighTrunc: 19531250,
				payoutSatsLowTrunc: 133334,
				enableMutualRedemption: 1,
			},
			settlement: {
				spendingTransaction: '35a53df7dc5e4156d676f781f829eb26f7e759301da4285844c164a9be12e841',
				settlementType: SettlementType.MUTUAL,
				hedgeSatoshis: 100000,
				longSatoshis: 33334,
			},
		};

		// Check that the actual data matches the expected data.
		expect(isParsedMutualRedemptionData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(expectedSettlementData);
	});

	test('should successfully parse a payout transaction', async () =>
	{
		// Make a copy of the settlement transaction.
		const transaction = { ...payoutTransaction };

		// Encode the transaction.
		const encodedTransaction = binToHex(encodeTransaction(transaction));

		// Parse the settlement transaction.
		const parsedSettlementData = await parseSettlementTransaction(encodedTransaction);

		// Define the expected data.
		const expectedSettlementData =
		{
			address: 'bitcoincash:pzn6kmrshz9qhzv6wpamagwpxefpwg2hhu09slsn70',
			funding: {
				fundingTransaction: 'a87ec10588d8d2a31cfc8be76b71bbcc6abf06645667305a1939ad82a69323f0',
				fundingOutput: 0,
			},
			parameters: {
				lowLiquidationPrice: 37500,
				highLiquidationPrice: 500000,
				startTimestamp: 1626435000,
				maturityTimestamp: 1626435120,
				oraclePublicKey: '03e4a5b4a1d9365492dfcad9a05adcdea259acb51a20eabb84584f7aa3c7009ebe',
				hedgeLockScript: '1976a914e4ebd82d22fc616a9f6b17d24e53dd22fe3de69a88ac',
				longLockScript: '1976a91469981c844c2055c94700568f2377515f48503ba288ac',
				lowTruncatedZeroes: '',
				highLowDeltaTruncatedZeroes: '00',
				nominalUnitsXSatsPerBchHighTrunc: 19531250,
				payoutSatsLowTrunc: 133334,
				enableMutualRedemption: 1,
			},
			settlement: {
				spendingTransaction: 'ac0a3e8ca56c53c212b8fc2829b0716b74e5af89e3a3d4e074f9f8a6cd0f393b',
				settlementType: SettlementType.MATURATION,
				hedgeSatoshis: 100002,
				longSatoshis: 33334,
				oraclePublicKey: '03e4a5b4a1d9365492dfcad9a05adcdea259acb51a20eabb84584f7aa3c7009ebe',
				settlementMessage: '306ef160020000000200000050c30000',
				settlementSignature: '2f9dc049b87e4f695b4ba19f57df42d44ecc27cff38a52f25d4cc537043278d805a582e0a147d580d8afa2b4cc73d8caa774d8829b3d29bced5027cb0cc2578b',
				previousMessage: '1c6ef160010000000100000050c30000',
				previousSignature: 'bb8e023d53ec4ed9011fb2b21da9f2363880292fd73e65895f8d1d42d34f9c999fd28f6d7e18b803c75c1a3508431ff15882ed6af94262b36cb9cde717ddeaf5',
				settlementPrice: 50000,
			},
		};

		// Check that the actual data matches the expected data.
		expect(isParsedPayoutData(parsedSettlementData)).toBeTruthy();
		expect(parsedSettlementData).toEqual(expectedSettlementData);
	});

	// For the following tests we need to hand-craft a P2SH transaction that looks like an AnyHedge transaction, but isn't.
	// This is currently not worth the effort as long as the function works correctly for *actual* AnyHedge transactions.
	test.todo('should throw when mutual redemption transaction redeem script does not match AnyHedge');
	test.todo('should throw when payout transaction redeem script does not match AnyHedge');
});
