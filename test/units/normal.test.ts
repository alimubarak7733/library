import { UseCase, useCases } from '../fixture/use-cases';
import { AnyHedgeManager } from '../../lib';

// Create an instance of the contract manager.
const contractManager = new AnyHedgeManager();

// Declare useCase as a global-scope reference variable.
let useCase: UseCase;

const testContractCreate = async function(): Promise<void>
{
	// Create a new contract.
	// @ts-ignore
	const contractData = await contractManager.createContract(useCase.contract.createContract.input);

	// Verify that the contract data matches expectations.
	expect(contractData).toEqual(useCase.contract.createContract.output);
};

const testContractValidate = async function(): Promise<void>
{
	// Validate contract address.
	// @ts-ignore
	const contractValidity = await contractManager.validateContract(useCase.contract.validateContract.input);

	// Verify that the contract is valid.
	expect(contractValidity).toEqual(useCase.contract.validateContract.output);
};

const testContractSimulateLiquidation = async function(): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contractManager.calculateSettlementOutcome(...useCase.liquidation.calculateMaturationOutcome.input);

	// Verify that the simulation results matches expectations.
	expect(simulationResults).toEqual(useCase.liquidation.calculateMaturationOutcome.output);
};

const testContractSimulateMaturation = async function(): Promise<void>
{
	// Simulate contract outcome.
	// @ts-ignore
	const simulationResults = await contractManager.calculateSettlementOutcome(...useCase.maturation.calculateMaturationOutcome.input);

	// Verify that the simulation results matches expectations.
	expect(simulationResults).toEqual(useCase.maturation.calculateMaturationOutcome.output);
};

// Set up normal tests.
const runNormalTests = async function(): Promise<void>
{
	// For each use case to test..
	for(const currentUseCase in useCases)
	{
		// .. assign it to the use case global reference.
		useCase = useCases[currentUseCase];

		// Test top-level non-stubbed library functions in parallel with the current use case.
		test('Create a contract', testContractCreate);
		test('Validate a contract', testContractValidate);
		test('Simulate a contract liquidation', testContractSimulateLiquidation);
		test('Simulate a contract maturation', testContractSimulateMaturation);

		// Test top-level stubbed library functions in series with the current use case.
		// NOTE: We currently don't have any stubbed library functions.
	}
};

const runTests = async function(): Promise<void>
{
	await runNormalTests();
};

runTests();
