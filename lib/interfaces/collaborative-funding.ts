import { Utxo } from 'cashscript';
import { ContractData } from './contract-data';

export interface UnsignedFundingProposal
{
	contractData: ContractData;
	utxo: Utxo;
}

export interface SignedFundingProposal extends UnsignedFundingProposal
{
	publicKey: string;
	signature: string;
}
