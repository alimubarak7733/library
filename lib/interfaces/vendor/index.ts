import { OracleData } from '@generalprotocols/price-oracle/dist/typings';

export type OraclePriceMessage = ReturnType<typeof OracleData.parsePriceMessage>;
