import { ContractFunding } from '../../common';
import { ContractMetadataV1, ContractParametersV1 } from './contract-data';

export interface ContractCreationParametersV1
{
	oraclePublicKey: string;
	hedgePublicKey: string;
	longPublicKey: string;
	hedgeAddress?: string;
	longAddress?: string;
	enableMutualRedemption?: number;
	nominalUnits: number;
	startPrice: number;
	startTimestamp: number;
	duration: number;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
}

export interface ContractRegistrationParametersV1 extends ContractCreationParametersV1
{
	feeAddress?: string;
	allowAddressReuse?: boolean;
}

export interface ContractValidationParametersV1 extends ContractCreationParametersV1
{
	contractAddress: string;
}

export interface ContractSettlementParametersV1
{
	oraclePublicKey: string;
	settlementMessage: string;
	settlementSignature: string;
	previousMessage: string;
	previousSignature: string;
	contractFunding: ContractFunding;
	contractMetadata: ContractMetadataV1;
	contractParameters: ContractParametersV1;
}

export interface AutomatedPayoutParametersV1
{
	oraclePublicKey: string;
	settlementMessage: string;
	settlementSignature: string;
	previousMessage: string;
	previousSignature: string;
	hedgePayoutSats: number;
	longPayoutSats: number;
	contractFunding: ContractFunding;
	contractParameters: ContractParametersV1;
}
