import { ContractFees, ContractFunding, SettlementType } from '../../common';

export interface ContractParametersV1
{
	lowLiquidationPrice: number;
	highLiquidationPrice: number;
	startTimestamp: number;
	maturityTimestamp: number;
	oraclePublicKey: string;
	hedgeLockScript: string;
	longLockScript: string;
	hedgeMutualRedeemPublicKey: string;
	longMutualRedeemPublicKey: string;
	enableMutualRedemption: number;
	lowTruncatedZeroes: string;
	highLowDeltaTruncatedZeroes: string;
	nominalUnitsXSatsPerBchHighTrunc: number;
	payoutSatsLowTrunc: number;
}

export interface ContractMetadataV1
{
	oraclePublicKey: string;
	hedgePublicKey: string;
	longPublicKey: string;
	hedgeAddress: string;
	longAddress: string;
	enableMutualRedemption: number;
	startTimestamp: number;
	duration: number;
	highLiquidationPriceMultiplier: number;
	lowLiquidationPriceMultiplier: number;
	startPrice: number;
	nominalUnits: number;
	longInputUnits: number;
	totalInputSats: number;
	hedgeInputSats: number;
	longInputSats: number;
	dustCost: number;
	minerCost: number;
}

export interface ContractHashesV1
{
	mutualRedemptionDataHash: string;
	payoutDataHash: string;
}

export interface ContractSettlementV1
{
	spendingTransaction: string;
	settlementType: SettlementType;
	hedgeSatoshis: number;
	longSatoshis: number;
	oraclePublicKey?: string;
	settlementMessage?: string;
	settlementSignature?: string;
	previousMessage?: string;
	previousSignature?: string;
	settlementPrice?: number;
}

export interface ContractDataV1
{
	version: string;
	address: string;
	parameters: ContractParametersV1;
	metadata: ContractMetadataV1;
	fee?: ContractFees;
	funding?: ContractFunding[];
	settlement?: ContractSettlementV1[];
}
