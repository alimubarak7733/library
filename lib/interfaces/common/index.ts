import { NetworkProvider } from 'cashscript';
import { ElectrumCluster } from 'electrum-cash';

export type ContractVersion = 'AnyHedge v0.11';

/**
 * Constructor parameters for setting up an anyhedge manager.
 */
export type AnyHedgeManagerConfig =
{
	/** authentication token used to authenticate network requests to the settlement service */
	authenticationToken?: string;

	/** string denoting which AnyHedge contract version to use. */
	contractVersion?: ContractVersion;

	/** fully qualified domain name for the settlement service provider. */
	serviceDomain?: string;

	/** network port number for the settlement service provider. */
	servicePort?: number;

	/** network scheme for the settlement service provider, either 'http' or 'https'. */
	serviceScheme?: 'http' | 'https';

	/** electrum cluster to use in BCH network operations. */
	electrumCluster?: ElectrumCluster;

	/** network provider to use for BCH network operations. */
	networkProvider?: NetworkProvider;
};

/**
 * Information about service fees.
 */
export type ContractFees =
{
	/** bitcoin cash address where service fees are to be paid. */
	address: string;

	/** how many satoshis are required to be paid as service fee. */
	satoshis: number;
};

export interface ContractFunding
{
	fundingTransaction: string;
	fundingOutput: number;
	fundingSatoshis: number;
	feeOutput?: number;
	feeSatoshis?: number;
}

export enum SettlementType
// eslint-disable-next-line @typescript-eslint/indent
{
	MATURATION = 'maturation',
	LIQUIDATION = 'liquidation',
	MUTUAL = 'mutual',
	EXTERNAL = 'external',
}

export interface SimulationOutput
{
	hedgePayoutSats: number;
	longPayoutSats: number;
	payoutSats: number;
	minerFeeSats: number;
}
