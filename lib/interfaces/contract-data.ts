import { ContractDataV1, ContractHashesV1, ContractMetadataV1, ContractParametersV1, ContractSettlementV1 } from './versions/v1';

// Combine the interface of different versions
export type ContractParameters = ContractParametersV1;
export type ContractMetadata = ContractMetadataV1;
export type ContractHashes = ContractHashesV1;
export type ContractSettlement = ContractSettlementV1;
export type ContractData = ContractDataV1;
