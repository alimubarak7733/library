import { ContractFunding, SettlementType } from './common';
import { ContractParameters, ContractSettlement } from './contract-data';

// Parsing a transaction gives us no information about the omitted properties.
export type ParsedFunding = Omit<ContractFunding, 'fundingSatoshis' | 'feeOutput' | 'feeSatoshis'>;

// Parsing a transaction can contain any of the fields of a ContractSettlement.
// Technically it should always contain spendingTransaction and settlementType though.
export type ParsedSettlement = Pick<ContractSettlement, 'spendingTransaction' | 'settlementType'> & Partial<ContractSettlement>;

// Parsing a mutual redemption does not give us access to the parameters hidden behind the payoutDataHash.
export type ParsedMutualRedemptionParameters = Omit<ContractParameters, 'oraclePublicKey' | 'hedgeLockScript' | 'longLockScript'>;

// Parsing a payout does not give us access to the parameters hidden behind the mutualRedemptionDataHash.
export type ParsedPayoutParameters = Omit<ContractParameters, 'hedgeMutualRedeemPublicKey' | 'longMutualRedeemPublicKey'>;

// Parsing just a redeem script does not give us access to any of the parameters hidden behind data hashes.
export type ParsedParametersCommon = Omit<ParsedMutualRedemptionParameters, 'hedgeMutualRedeemPublicKey' | 'longMutualRedeemPublicKey'>;

export interface ParsedMutualRedemptionData
{
	address: string;
	funding: ParsedFunding;
	settlement: ParsedSettlement;
	parameters: ParsedMutualRedemptionParameters;
}

export interface ParsedPayoutData
{
	address: string;
	funding: ParsedFunding;
	settlement: ParsedSettlement;
	parameters: ParsedPayoutParameters;
}

export type ParsedSettlementData = ParsedMutualRedemptionData | ParsedPayoutData;

// Determine if the type of the parsed settlement data is a mutual redemption.
export const isParsedMutualRedemptionData = function(data: ParsedSettlementData): data is ParsedMutualRedemptionData
{
	return data.settlement.settlementType === SettlementType.MUTUAL;
};

// Determine if the type of the parsed settlement data is a payout.
export const isParsedPayoutData = function(data: ParsedSettlementData): data is ParsedPayoutData
{
	return data.settlement.settlementType === SettlementType.MATURATION || data.settlement.settlementType === SettlementType.LIQUIDATION;
};
