import { addressContentsToLockingBytecode, AddressType, bigIntToBinUint64LE, binToBigIntUint64LE, binToHex, cashAddressToLockingBytecode, createTransactionContextCommon, encodeTransaction, generateSigningSerializationBCH, hexToBin, Input, instantiateSecp256k1, instantiateSha256, Output, Sha256, SigningSerializationFlag, Transaction } from '@bitauth/libauth';
import { utils, Utxo } from 'cashscript';
import { P2PKH_INPUT_SIZE } from '../constants';
import { ContractData, SignedFundingProposal, UnsignedFundingProposal } from '../interfaces';
import { calculateTotalSats } from './anyhedge-util';
import { decodeWIF, derivePublicKey, hash160 } from './bitcoincash-util';
import { debug } from './javascript-util';

interface InputWithValue extends Input
{
	satoshis: number;
}

interface TransactionWithInputValue extends Transaction
{
	inputs: InputWithValue[];
}

interface DecodedUnlockingBytecodeP2PKH
{
	publicKey: string;
	signature: string;
}

/**
 * Encode a public key and signature into unlocking bytecode for a P2PKH locking bytecode.
 *
 * @param publicKey   public key to use in the unlocking bytecode.
 * @param signature   signature to use in the unlocking bytecode.
 *
 * @returns unlocking bytecode for the provided parameters.
 */
export const encodeUnlockingBytecodeP2PKH = function(publicKey: string, signature: string): Uint8Array
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'encodeUnlockingBytecodeP2PKH() <=', arguments ]);

	// Encode the signature and public key into unlocking bytecode.
	const unlockingBytecode = utils.scriptToBytecode([ hexToBin(signature), hexToBin(publicKey) ]);

	// Output function result for easier collection of test data.
	debug.result([ 'encodeUnlockingBytecodeP2PKH() =>', unlockingBytecode ]);

	return unlockingBytecode;
};

/**
 * Decode a P2PKH unlocking bytecode into the corresponding signature and public key.
 *
 * @param unlockingBytecode   unlocking bytecode for a P2PKH locking bytecode.
 *
 * @returns public key and signature for the provided unlocking bytecode.
 */
export const decodeUnlockingBytecodeP2PKH = function(unlockingBytecode: Uint8Array): DecodedUnlockingBytecodeP2PKH
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'decodeUnlockingBytecodeP2PKH() <=', arguments ]);

	// Extract the signature and public key from the unlocking bytecode
	const [ signatureBin, publicKeyBin ] = utils.bytecodeToScript(unlockingBytecode);

	// Convert the signature and public key to hex and put them in an object
	const decodedUnlockingBytecode =
	{
		publicKey: binToHex(publicKeyBin as Uint8Array),
		signature: binToHex(signatureBin as Uint8Array),
	};

	// Output function result for easier collection of test data.
	debug.result([ 'decodeUnlockingBytecodeP2PKH() =>', decodedUnlockingBytecode ]);

	return decodedUnlockingBytecode;
};

/**
 * Convert a "UTXO" object to a Libauth "Input" object (with value preserved).
 *
 * @param utxo         UTXO object to be converted.
 * @param [sequence]   sequence number for the input (default 0xffffffff to indicate absence of time locks).
 *
 * @returns Libauth Input object for the UTXO.
 */
export const createUnsignedInput = function(utxo: Utxo, sequence: number = 0xffffffff): InputWithValue
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'createUnsignedInput() <=', arguments ]);

	// Construct an unsigned input from the UTXO (including the value in satoshis)
	const unsignedInput =
	{
		outpointTransactionHash: hexToBin(utxo.txid),
		outpointIndex: Number(utxo.vout),
		sequenceNumber: sequence,
		unlockingBytecode: new Uint8Array(),
		satoshis: utxo.satoshis,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'createUnsignedInput() =>', unsignedInput ]);

	return unsignedInput;
};

/**
 * Create a Libauth Output object from an address and amount.
 *
 * @param address    address to encode in the output.
 * @param satoshis   satoshi amount to encode in the output.
 *
 * @throws {Error} if address cannot be decoded.
 * @returns Libauth Output object for the address and amount.
 */
export const createOutput = function(address: string, satoshis: number): Output
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'createOutput() <=', arguments ]);

	// Generate the locking script for the passed address.
	const lockScriptResult = cashAddressToLockingBytecode(address);

	// If lockScriptResult is a string, it represents an error, so we throw it.
	if(typeof lockScriptResult === 'string') throw(new Error(lockScriptResult));

	// Extract the bytecode (locking script) from the lockScriptResult.
	const lockingBytecode = lockScriptResult.bytecode;

	// Convert the amount to a BCH script number.
	const satoshisBin = bigIntToBinUint64LE(BigInt(satoshis));

	// Assemble the output.
	const output = { lockingBytecode, satoshis: satoshisBin };

	// Output function result for easier collection of test data.
	debug.result([ 'createOutput() =>', output ]);

	return output;
};

/**
 * Constructs the required outputs for a funding transaction, including fee output if fee information is provided.
 *
 * @param contractData   contract data for the contract.
 *
 * @returns {Output[]} list of Libauth-compatible Outputs for the funding transaction
 */
export const constructFundingOutputs = function(contractData: ContractData): Output[]
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'constructFundingOutputs() <=', arguments ]);

	// Create an output to the contract address for the required total contract satoshis
	const outputs = [ createOutput(contractData.address, calculateTotalSats(contractData.metadata)) ];

	// Add a fee output for the required amount if fee information is present
	if(contractData.fee)
	{
		outputs.push(createOutput(contractData.fee.address, contractData.fee.satoshis));
	}
	// Output function result for easier collection of test data.
	debug.result([ 'constructFundingOutputs() =>', outputs ]);

	return outputs;
};

/**
 * Generate a signing serialization based on the provided transaction / input related parameters.
 *
 * @param transaction            transaction to generate the signing serialization for.
 * @param inputSatoshis          amount of satoshis of the spending input.
 * @param inputIndex             index of the spending input.
 * @param inputLockingBytecode   locking bytecode of the spending input.
 * @param hashType               hash type used in generating the signing serialization.
 * @param sha256                 Libauth Sha256 object.
 *
 * @returns double SHA256 hash of the signing serialization generated for the specified parameters.
 */
export const generateSigningSerializationHash = function(
	transaction: Transaction,
	inputSatoshis: number,
	inputIndex: number,
	inputLockingBytecode: Uint8Array,
	signingSerializationType: Uint8Array,
	sha256: Sha256,
): Uint8Array
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'generateSigningSerializationHash() <=', arguments ]);

	// Generate a Libauth "transaction context" for the transaction.
	const transactionContext = createTransactionContextCommon({
		inputIndex,
		sourceOutput: { satoshis: bigIntToBinUint64LE(BigInt(inputSatoshis)) },
		spendingTransaction: transaction,
	});

	// Generate the signing serialization from this transaction context and additional data.
	const signingSerialization = generateSigningSerializationBCH({
		// Provide the data generated in the transaction context
		correspondingOutput: transactionContext.correspondingOutput,
		locktime: transactionContext.locktime,
		outpointIndex: transactionContext.outpointIndex,
		outpointTransactionHash: transactionContext.outpointTransactionHash,
		outputValue: transactionContext.outputValue,
		sequenceNumber: transactionContext.sequenceNumber,
		transactionOutpoints: transactionContext.transactionOutpoints,
		transactionOutputs: transactionContext.transactionOutputs,
		transactionSequenceNumbers: transactionContext.transactionSequenceNumbers,

		// Provide the locking bytecode for the provided input
		coveredBytecode: inputLockingBytecode,

		// Provide a libauth sha256 object
		sha256,

		// Provide the signing serialization type
		signingSerializationType,

		// Use transaction version 2
		version: 2,
	});

	// Take the double SHA256 of the signing serialization to sign.
	const messageHash = sha256.hash(sha256.hash(signingSerialization));

	// Output function result for easier collection of test data.
	debug.result([ 'generateSigningSerializationHash() =>', messageHash ]);

	return messageHash;
};

/**
 * Sign an unsigned transaction using the provided private key WIF, skipping inputs that are already signed.
 *
 * @param privateKeyWIF         private key WIF to use for signing.
 * @param unsignedTransaction   transaction object to be signed.
 *
 * @returns signed transaction.
 */
export const signTransactionP2PKH = async function(privateKeyWIF: string, unsignedTransaction: TransactionWithInputValue): Promise<Transaction>
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'signTransactionP2PKH() <=', arguments ]);

	// Instantiate crypto objects.
	const secp256k1 = await instantiateSecp256k1();
	const sha256 = await instantiateSha256();

	// Decode WIF into a private key.
	const privateKeyHex = await decodeWIF(privateKeyWIF);

	// Derive public key from the private key.
	const publicKeyHex = await derivePublicKey(privateKeyWIF);

	// Hash the public key.
	const publicKeyHashBin = await hash160(hexToBin(publicKeyHex));

	// Generate the inputs' locking bytecode based on the provided public key.
	const addressContents = { payload: publicKeyHashBin, type: AddressType.p2pkh };
	const previousLockingBytecode = addressContentsToLockingBytecode(addressContents);

	// Use the SIGHASH_ALL | SIGHASH_ANYONE_CAN_PAY flags so that extra inputs may be added by other participants.
	// Also include the SIGHASH_FORK_ID flag, which is required for all transactions on BCH.
	const hashType = Uint8Array.of(SigningSerializationFlag.allOutputs | SigningSerializationFlag.singleInput | SigningSerializationFlag.forkId);

	// Sign all inputs.
	const signedInputs = unsignedTransaction.inputs.map((input, inputIndex) =>
	{
		// Skip inputs that are already signed.
		if(binToHex(input.unlockingBytecode) !== '')
		{
			return input;
		}

		// Generate a signing serialization hash to be signed.
		const signingSerializationHash = generateSigningSerializationHash(
			unsignedTransaction,
			input.satoshis,
			inputIndex,
			previousLockingBytecode,
			hashType,
			sha256,
		);

		// Schnorr sign the message hash using the provided private key.
		const signature = secp256k1.signMessageHashSchnorr(hexToBin(privateKeyHex), signingSerializationHash);

		// Append the hashtype to the signature.
		const transactionSignature = new Uint8Array([ ...signature, ...hashType ]);

		// Encode the public key and transaction signature into unlocking bytecode.
		const unlockingBytecode = encodeUnlockingBytecodeP2PKH(publicKeyHex, binToHex(transactionSignature));

		// Add the unlocking bytecode to the input.
		const signedInput = { ...input, unlockingBytecode };

		return signedInput;
	});

	// Add the signed inputs to the transaction.
	const signedTransaction = { ...unsignedTransaction, inputs: signedInputs };

	// Output function result for easier collection of test data.
	debug.result([ 'signTransactionP2PKH() =>', signedTransaction ]);

	return signedTransaction;
};

/**
 * Deterministically generate a partial transaction from a funding proposal.
 *
 * @param fundingProposal   signed or unsigned funding proposal to generate a transaction for.
 *
 * @returns partial (signed or unsigned) transaction for funding proposal.
 */
export const generateTransactionFromFundingProposal = function(
	fundingProposal: UnsignedFundingProposal | SignedFundingProposal,
): TransactionWithInputValue
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'generateTransactionFromFundingProposal() <=', arguments ]);

	// Determine outputs required for the funding transaction.
	const fundingOutputs = constructFundingOutputs(fundingProposal.contractData);

	// Transform the proposal's UTXO to an unsigned "Input" object.
	const input = createUnsignedInput(fundingProposal.utxo);

	// If the proposal is signed, we add the encoded unlocking bytecode to the input.
	if('signature' in fundingProposal && 'publicKey' in fundingProposal)
	{
		input.unlockingBytecode = encodeUnlockingBytecodeP2PKH(fundingProposal.publicKey, fundingProposal.signature);
	}
	// If only part of the unlocking data is present in the proposal, we throw an error.
	else if('signature' in fundingProposal || 'publicKey' in fundingProposal)
	{
		throw(new Error('Incomplete unlocking data provided in funding proposal'));
	}

	// Construct a partial unsigned or signed transaction.
	// Note that we set locktime to zero to disable time locks.
	const transaction =
	{
		version: 2,
		inputs: [ input ],
		outputs: fundingOutputs,
		locktime: 0,
	};

	// Output function result for easier collection of test data.
	debug.result([ 'generateTransactionFromFundingProposal() =>', transaction ]);

	return transaction;
};

/**
 * Merge the inputs of transaction 2 into transaction 1.
 *
 * @param transaction1   first partial transaction.
 * @param transaction2   second partial transaction.
 *
 * @returns complete transaction.
 */
export const mergeTransactionInputs = function(
	transaction1: Transaction,
	transaction2: Transaction,
): Transaction
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'mergeTransactionInputs() <=', arguments ]);

	// Merge inputs for both transactions.
	const mergedInputs = [ ...transaction1.inputs, ...transaction2.inputs ];

	// Add the merged inputs to transaction 1.
	const mergedTransaction = { ...transaction1, inputs: mergedInputs };

	// Output function result for easier collection of test data.
	debug.result([ 'mergeTransactionInputs() =>', mergedTransaction ]);

	return mergedTransaction;
};

/**
 * Calculate the size of the passed transaction in bytes
 *
 * @param transaction   transaction to calculate the size of
 *
 * @returns size of the passed transaction in bytes
 */
export const calculateTransactionSize = function(transaction: Transaction): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateTransactionSize() <=', arguments ]);

	// Calculate the byte length of the encoded transaction
	const transactionSize = encodeTransaction(transaction).byteLength;

	// Output function result for easier collection of test data.
	debug.result([ 'calculateTransactionSize() =>', transactionSize ]);

	return transactionSize;
};

/**
 * Estimate the transaction size of a dummy transaction in bytes, disregarding inputs
 *
 * @param outputs   outputs of the transaction to estimate
 *
 * @returns estimation of the transaction size in bytes, disregarding inputs
 */
export const estimateTransactionSizeWithoutInputs = function(outputs: Output[]): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'estimateTransactionSizeWithoutInputs() <=', arguments ]);

	// Create an estimation transaction without inputs and dummy data for locktime and version
	const estimationTransaction = { inputs: [], outputs, locktime: 0, version: 0 };

	// Estimate the size of that transaction
	const estimatedTransactionSize = calculateTransactionSize(estimationTransaction);

	// Output function result for easier collection of test data.
	debug.result([ 'estimateTransactionSizeWithoutInputs() =>', estimatedTransactionSize ]);

	return estimatedTransactionSize;
};

/**
 * Calculate the total required satoshis to pay for a contract funding, including
 * potential settlement service fees and miner fees. For miner fee calculation
 * it is assumed that the transaction contains 2 P2PKH inputs.
 *
 * @param contractData   contract data to calculate funding satoshis for.
 *
 * @returns the total satoshis required as input to a funding transaction.
 */
export const calculateRequiredFundingSatoshis = function(contractData: ContractData): number
{
	// Output function call arguments for easier collection of test data.
	// eslint-disable-next-line prefer-rest-params
	debug.params([ 'calculateRequiredFundingSatoshis() <=', arguments ]);

	// Construct the funding outputs required (including potential fee output)
	const fundingOutputs = constructFundingOutputs(contractData);

	// Calculate the sum of the satoshis of all outputs
	const fundingSatoshis = fundingOutputs
		.reduce((totalSatoshis, { satoshis }) => totalSatoshis + Number(binToBigIntUint64LE(satoshis)), 0);

	// Estimate the expected transaction fees (based on the assumption of 2 P2PKH inputs)
	const minerFeeSatoshis = estimateTransactionSizeWithoutInputs(fundingOutputs) + 2 * P2PKH_INPUT_SIZE;

	// Add the funding satoshis and miner fee satoshis to get the total required satoshis
	const totalRequiredSatoshis = fundingSatoshis + minerFeeSatoshis;

	// Output function result for easier collection of test data.
	debug.result([ 'calculateRequiredFundingSatoshis() =>', totalRequiredSatoshis ]);

	return totalRequiredSatoshis;
};
