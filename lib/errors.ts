/* eslint-disable max-classes-per-file */

export class MissingAuthenticationTokenError extends Error
{
	constructor()
	{
		// Define a message indicating that no authentication token was provided.
		const message = 'No authentication token was provided. Please request one '
		+ 'using requestAuthenticationToken() and pass it into the AnyHedgeManager constructor.';

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

export class IncorrectWIFError extends Error
{
	constructor(privateKeyWIF: string)
	{
		// Define a message indicating that the WIF string has an incorrect format.
		const message = `Provided WIF string (${privateKeyWIF.substr(0, 4)}...) has an incorrect format`;

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}

export class SettlementParseError extends Error
{
	constructor(reason: string)
	{
		// Define a message indicating that a settlement transaction could not be parsed.
		const message = `Settlement transaction could not be parsed: ${reason}`;

		// Create an instance of the parent Error class using the specified message.
		super(message);
	}
}
